export default {
  /** JWT KEY */
  jwtSecret: process.env.JWT_SECRET || 'fonomed/rootsdev',
  /** DB CONFIG */
  DB: {
    URI: process.env.MONGODB_URI || 'mongodb://mongo:27017/fonom3d',
    USER: process.env.MONGODB_USER,
    PASSWORD: process.env.MONGODB_PASSWORD
  }
};