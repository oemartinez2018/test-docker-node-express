import { Router } from 'express'
import { getAll, getById } from '../controllers/user.controller'

const router = Router();

/** Todos los usuario :: ASC | DESC */
router.get('/doctores/:filtro', getAll);

/** Un usuario por ID */
router.get('/doctores/find/by/:id', getById);


export default router;