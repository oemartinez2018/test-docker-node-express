import { Router } from 'express'
import { signIn, enviarSMS, recibirSMS, signUp, update, updateStatus,
         verifySendEmail,verifyRecibirEmail, cambiarContrasenia } from '../controllers/user.controller'
import upload from '../libs/multer'

const router = Router();

/** Verificacion ENVIAR SMS */
router.get('/sendverify', enviarSMS);
/** Verificacion RECIBIR SMS */
router.get('/comprobarverify', recibirSMS);
/** Registro :: La foto es opcional */
router.route('/signup').post(upload.fields([{ name: 'documentos', maxCount: 5 },{ name: 'foto', maxCount: 1 }]), signUp);
/** Login :: Email & Password */
router.post('/signin', signIn);
/** Update perfil :: La foto es opcional */
router.route('/perfil/update/:id').post(upload.fields([{ name: 'documentos', maxCount: 5 },{ name: 'foto', maxCount: 1 }]), update);
/** Cambiar estado */
router.put('/perfil/update/status/:id', updateStatus);
/** Enviar email */
router.post('/enviarmail/:nombre/:email', verifySendEmail);
/** Recibir email */
router.post('/recibirmail/:email/:code', verifyRecibirEmail);
/** Cambio de contraseña */
router.put('/cambio-contrasenia', cambiarContrasenia);

export default router;