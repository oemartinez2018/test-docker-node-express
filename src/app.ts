const path = require('path');
import express from 'express'
import passport from 'passport'
import passportMiddleware from './middlewares/passport';
import cors from 'cors';
import morgan from 'morgan';
// const fileUpload = require('express-fileupload');
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './swagger.json';

import authRoutes from './routes/auth.routes';
import especialidadRoutes from './routes/especialidad.routes';
import userRoutes from './routes/user.routes';

const app = express();

// settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(passport.initialize());
passport.use(passportMiddleware);

app.get('/', (req, res) => {
  return res.send(`The API is at http://localhost:${app.get('port')}`);
})

app.use(express.static(path.join(__dirname, './public')));
// app.use(fileUpload());
app.use(authRoutes);
app.use(especialidadRoutes);
app.use(userRoutes);
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

export default app;
