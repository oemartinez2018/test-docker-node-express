import { model, Schema, Document } from "mongoose";

export interface ITarjeta extends Document {
  nombre: Number;
  mes: Number;
  anio: Number;
  cvv: Number;
  titular: string;
  
};

const tarjetaSchema = new Schema({

    numero: {
        type: Number,
        required: true
    },
    mes: {
        type: Number,
        required: true
    },
    anio: {
        type: Number,
        required: true
    },
    cvv: {
        type: Number,
        required: true
    },
    titular: {
        type: String,
        required: true
    }

}, { timestamps: true });


export default model<ITarjeta>("Tarjeta", tarjetaSchema);
