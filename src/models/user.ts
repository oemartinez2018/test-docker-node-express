import { model, Schema, Document } from "mongoose";
import bcrypt from "bcrypt";
import { IEspecialidad } from "./especialidad";
import { ITarjeta } from "./tarjeta";

export interface IUser extends Document {
  email: string;
  password: string;
  nombre: string;
  apellido: string;
  foto?: string;
  genero: string;
  fecha_nacimiento: string;
  telefono: string;
  documento_ui: string;
  tipo: string;
  jvmp?: string;
  tarifa_g?: string;
  tarifa_m?: string;
  aprobado: Boolean;
  create_at: Date;
  documentos?: [];
  especialidades: IEspecialidad[],
  tarjetas?: ITarjeta[],
  comparePassword: (password: string) => Promise<Boolean>

};

const userSchema = new Schema({

  nombre: {
    type: String,
    required: true
  },
  apellido: {
    type: String,
    required: true
  },
  foto: {
    type: String,
    required: false
  },
  genero: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  fecha_nacimiento: {
    type: String,
    required: true
  },
  telefono: {
    type: String,
    required: true
  },
  documento_ui: {
    type: String,
    required: true
  },
  tipo: {
    type: String,
    required: true
  },
  jvmp: {
    type: String,
    required: false
  },
  tarifa_g: {
    type: String,
    required: false
  },
  tarifa_m: {
    type: String,
    required: false
  },
  aprobado: {
    type: Boolean,
    default: false
  },
  create_at: {
    type: Date,
    default: Date.now
  },
  documentos: {
    type: Array
  },
  especialidades: [{
    type: Schema.Types.ObjectId,
    ref: 'Especialidad',
    require: false
  }],
  tarjetas: [{
    type: Schema.Types.ObjectId,
    ref: 'Tarjeta',
    require: false
  }]
}, { timestamps: true });

userSchema.pre<IUser>("save", async function (next) {
  const user = this;

  if (!user.isModified("password")) return next();

  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(user.password, salt);
  user.password = hash;

  next();
});

userSchema.methods.comparePassword = async function (
  password: string
): Promise<Boolean> {
  return await bcrypt.compare(password, this.password);
};

export default model<IUser>("User", userSchema);