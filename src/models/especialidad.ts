import { model, Schema, Document } from "mongoose";

export interface IEspecialidad extends Document {
  nombre: string;
};

const especialidadSchema = new Schema({

  nombre: {
    type: String,
    required: true
  }
}, { timestamps: true });


export default model<IEspecialidad>("Especialidad", especialidadSchema);
