"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
;
const tarjetaSchema = new mongoose_1.Schema({
    numero: {
        type: Number,
        required: true
    },
    mes: {
        type: Number,
        required: true
    },
    anio: {
        type: Number,
        required: true
    },
    cvv: {
        type: Number,
        required: true
    },
    titular: {
        type: String,
        required: true
    }
}, { timestamps: true });
exports.default = mongoose_1.model("Tarjeta", tarjetaSchema);
