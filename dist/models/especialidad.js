"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
;
const especialidadSchema = new mongoose_1.Schema({
    nombre: {
        type: String,
        required: true
    }
}, { timestamps: true });
exports.default = mongoose_1.model("Especialidad", especialidadSchema);
