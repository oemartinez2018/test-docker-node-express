"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
;
const verifySchema = new mongoose_1.Schema({
    code: {
        type: String
    },
    email: {
        type: String
    },
    nombre: {
        type: String
    }
}, { timestamps: true });
exports.default = mongoose_1.model("Verify", verifySchema);
