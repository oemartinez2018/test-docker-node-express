"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    /** JWT KEY */
    jwtSecret: process.env.JWT_SECRET || 'fonomed/rootsdev',
    /** DB CONFIG */
    DB: {
        URI: process.env.MONGODB_URI || 'mongodb://localhost/fonom3d',
        USER: process.env.MONGODB_USER,
        PASSWORD: process.env.MONGODB_PASSWORD
    }
};
