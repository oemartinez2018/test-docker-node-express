"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const especialidad_1 = __importDefault(require("../models/especialidad"));
/** TODAS LAS ESPECIALIDES */
exports.getAll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const especialidad = yield especialidad_1.default.find({});
    return res.json({
        errorCode: "0",
        errorMsg: "Servicio ejecutado con exito.",
        data: especialidad
    });
});
/** REGISTRO DE ESPECIALIDADES */
exports.nuevo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.body) {
        return res
            .status(400)
            .json({ errorCode: "404", errorMsg: "Completa los campos." });
    }
    const especialidad = yield especialidad_1.default.findOne({ nombre: req.body.nombre });
    if (especialidad) {
        return res.status(400).json({ errorCode: "404", errorMsg: "Esta especialidad ya a sido creada." });
    }
    const newEspecialidad = new especialidad_1.default(req.body);
    yield newEspecialidad.save();
    return res.status(201).json({
        errorCode: "0",
        errorMsg: "Servicio ejecutado con exito."
    });
});
/** ACTUALIZACION DE ESPECIALIDADES :: RECIBE EL ID */
exports.actualizar = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.body || !req.params.id) {
        return res
            .status(400)
            .json({ errorCode: "404", errorMsg: "Campos incompletos o falta parametros en la URL." });
    }
    const especialidad = yield especialidad_1.default.findOne({ nombre: req.body.nombre });
    if (especialidad) {
        return res.status(400).json({ errorCode: "404", errorMsg: "Esta especialidad ya a sido creada." });
    }
    const { nombre } = req.body;
    yield especialidad_1.default.findByIdAndUpdate(req.params.id, { nombre });
    return res.status(201).json({
        errorCode: "0",
        errorMsg: "Servicio ejecutado con exito."
    });
});
/** ELIMINACION DE ESPECIALIDADES :: RECIBE EL ID */
exports.eliminar = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.params.id) {
        return res
            .status(400)
            .json({ errorCode: "404", errorMsg: "Faltan parametros en URL." });
    }
    const especialidad = yield especialidad_1.default.findOne({ nombre: req.body.nombre });
    if (especialidad)
        return res.status(400).json({ errorCode: "404", errorMsg: "Esta especialidad ya a sido creada." });
    yield especialidad_1.default.findByIdAndDelete(req.params.id);
    return res.status(201).json({
        errorCode: "0",
        errorMsg: "Servicio ejecutado con exito."
    });
});
