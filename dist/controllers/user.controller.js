"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const config_1 = __importDefault(require("../config/config"));
const twilio_1 = __importDefault(require("../config/twilio"));
const functions_1 = require("../libs/functions");
const user_1 = __importDefault(require("../models/user"));
const verify_1 = __importDefault(require("../models/verify"));
const client = require('twilio')(twilio_1.default.accountSID, twilio_1.default.authToken);
/** GENERACION DE TOKEN CON JWT */
function createToken(user) {
    return jsonwebtoken_1.default.sign({ id: user.id, email: user.email }, config_1.default.jwtSecret, {
        expiresIn: 86400
    });
}
function createNewPassword(password) {
    return __awaiter(this, void 0, void 0, function* () {
        const salt = yield bcrypt_1.default.genSalt(10);
        const hash = yield bcrypt_1.default.hash(password, salt);
        return hash;
    });
}
;
/** CAMBIAR CONTRASEÑA */
exports.cambiarContrasenia = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { email, password, r_password } = req.body;
    if (password == r_password) {
        const user = yield user_1.default.findOne({ email: email });
        if (user) {
            yield user_1.default.findByIdAndUpdate(user._id, { password: yield createNewPassword(password) });
            return res.status(200).json({
                code: "200",
                message: "Contraseña cambiada con exito."
            });
        }
        else {
            return res.status(404).json({
                code: "404",
                message: "Algo salio mal.",
            });
        }
    }
    else {
        return res.status(404).json({
            code: "404",
            message: "Contraseñas no son iguales.",
        });
    }
});
/** RECIBIR EMAIL :: EMAIL, CODE */
exports.verifyRecibirEmail = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const code = req.params.code;
    const email = req.params.email;
    const verify = yield verify_1.default.findOne({ code: code, email: email });
    if (verify) {
        yield verify_1.default.findByIdAndDelete(verify._id);
        return res.status(200).json({
            code: "200",
            message: "Codigo valido."
        });
    }
    else {
        return res.status(404).json({
            code: "404",
            message: "Codigo no valido.",
        });
    }
});
/** SEND EMAIL :: NOMBRE, EMAIL */
exports.verifySendEmail = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const nombre = req.params.nombre;
    const email = req.params.email;
    const verify = yield verify_1.default.findOne({ email: email });
    if (verify) {
        functions_1.sendEmail(verify.nombre, verify.email, verify.code);
        return res.status(200).json({
            code: "200",
            message: "Se a enviado nuevamente el codigo al correo.",
        });
    }
    else {
        let code = functions_1.generateCode();
        functions_1.sendEmail(nombre, email, code);
        const newVerify = new verify_1.default({ nombre, email, code });
        yield newVerify.save();
        return res.status(201).json({
            code: "201",
            message: "Se a enviado el codigo al correo.",
        });
    }
});
/** VERIFICACION SEND SMS */
exports.enviarSMS = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    client
        .verify
        .services(twilio_1.default.serviceSID)
        .verifications
        .create({
        to: `+${req.query.phonenumber}`,
        channel: req.query.channel === 'call' ? 'call' : 'sms'
    })
        .then(data => {
        res.status(201).send({
            message: "Se a enviado el codigo al telefono.",
            phonenumber: req.query.phonenumber,
            info: data
        });
    });
});
/** VERIFICACION RECIBIR SMS */
exports.recibirSMS = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    client
        .verify
        .services(twilio_1.default.serviceSID)
        .verificationChecks
        .create({
        to: `+${req.query.phonenumber}`,
        code: req.query.code
    })
        .then(data => {
        if (data.status === "approved") {
            res.status(200).send({
                code: "200",
                message: "Codigo valido.",
                info: data
            });
        }
        else {
            res.status(404).send({
                code: "404",
                message: "Codigo no valido.",
                info: data
            });
        }
    });
});
/** REGISTRO DE USUARIOS */
exports.signUp = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    /** Campos requeridos */
    if (!req.body.email || !req.body.password) {
        return res
            .status(403)
            .json({ code: "403", message: "Completa los campos." });
    }
    /** Constrasenas iguales */
    if (!req.body.password || !req.body.r_password) {
        return res
            .status(403)
            .json({ code: "403", message: "Contrasena no son iguales." });
    }
    /** Existencia de usuario */
    const user = yield user_1.default.findOne({ email: req.body.email });
    if (user)
        return res.status(400).json({ code: "400", message: "Este correo ya esta asociado a una cuenta." });
    /** Recepcion de datos */
    const { nombre, apellido, email, password, genero, fecha_nacimiento, telefono, documento_ui, tipo, especialidades, jvmp, tarifa_g, tarifa_m, } = req.body;
    /** Validacion (foto) y persistir */
    if (req.files['foto']) {
        if (req.files['documentos']) {
            let element = [];
            for (let index = 0; index < req.files['documentos'].length; index++) {
                element.push(req.files['documentos'][index]['path']);
            }
            const newUser = new user_1.default({
                nombre, apellido, email, password, genero, documentos: element,
                fecha_nacimiento, telefono, documento_ui, tipo,
                especialidades, jvmp, tarifa_g, tarifa_m, foto: req.files['foto'][0]["path"]
            });
            const userNew = yield newUser.save();
            return res.status(201).json(userNew);
        }
        else {
            const newUser = new user_1.default({
                nombre, apellido, email, password, genero,
                fecha_nacimiento, telefono, documento_ui, tipo,
                especialidades, jvmp, tarifa_g, tarifa_m, foto: req.files['foto'][0]["path"]
            });
            const userNew = yield newUser.save();
            return res.status(201).json(userNew);
        }
    }
    else {
        if (req.files['documentos']) {
            let element = [];
            for (let index = 0; index < req.files['documentos'].length; index++) {
                element.push(req.files['documentos'][index]['path']);
            }
            const newUser = new user_1.default({
                nombre, apellido, email, password, genero, documentos: element,
                fecha_nacimiento, telefono, documento_ui, tipo,
                especialidades, jvmp, tarifa_g, tarifa_m, foto: `https://ui-avatars.com/api/?name=${nombre}+${apellido}`
            });
            const userNew = yield newUser.save();
            return res.status(201).json(userNew);
        }
        else {
            const newUser = new user_1.default({
                nombre, apellido, email, password, genero,
                fecha_nacimiento, telefono, documento_ui, tipo,
                especialidades, jvmp, tarifa_g, tarifa_m, foto: `https://ui-avatars.com/api/?name=${nombre}+${apellido}`
            });
            const userNew = yield newUser.save();
            return res.status(201).json(userNew);
        }
    }
});
/** ACTUALIZAR PERFIL :: ENVIAR ID POR URL */
exports.update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    /** Campos requeridos */
    if (!req.body.email ||
        !req.body.nombre ||
        !req.body.apellido ||
        !req.body.genero ||
        !req.body.telefono) {
        return res
            .status(403)
            .json({ code: "403", message: "Completa los campos." });
    }
    /** Existencia de usuario */
    const user = yield user_1.default.findById({ _id: req.params.id });
    if (!user)
        return res.status(404).json({ code: "404", message: "Usuario no existe." });
    /** Recepcion de datos */
    const { nombre, apellido, email, password, genero, fecha_nacimiento, telefono, documento_ui, tipo, especialidades, jvmp, tarifa_g, tarifa_m, } = req.body;
    /** Validacion (foto) y persistir (update) */
    if (req.file) {
        const updateUser = yield user_1.default.findByIdAndUpdate(req.params.id, {
            nombre, apellido, email, password, genero,
            fecha_nacimiento, telefono, documento_ui, tipo,
            especialidades, jvmp, tarifa_g, tarifa_m, foto: req.file.path
        });
        return res.status(200).json(updateUser);
    }
    else {
        const updateUser = yield user_1.default.findByIdAndUpdate(req.params.id, {
            nombre, apellido, email, password, genero,
            fecha_nacimiento, telefono, documento_ui, tipo,
            especialidades, jvmp, tarifa_g, tarifa_m, foto: `https://ui-avatars.com/api/?name=${nombre}+${apellido}`
        });
        return res.status(200).json(updateUser);
    }
});
/** ACTUALIZAR ESTADO :: ENVIAR ID POR URL */
exports.updateStatus = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    /** Existencia de usuario */
    const user = yield user_1.default.findById({ _id: req.params.id });
    if (!user) {
        return res.status(404).json({ code: "404", message: "Usuario no existe." });
    }
    /** Recepcion de datos */
    const { aprobado } = req.body;
    /** Persistir (Update) */
    const updateUser = yield user_1.default.findByIdAndUpdate(req.params.id, { aprobado });
    return res.status(200).json(updateUser);
});
/** LOGIN :: EMAIL Y PASSWORD */
exports.signIn = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    /** Campos requeridos */
    if (!req.body.email || !req.body.password) {
        return res
            .status(403)
            .json({ code: "403", message: "Completa los campos." });
    }
    /** Existencia de usuario */
    const user = yield user_1.default.findOne({ email: req.body.email });
    if (!user) {
        return res.status(404).json({ code: "404", message: "El correo no esta asociado a ninguna cuenta." });
    }
    /** Match de contrasena, validacion de Bcrypt */
    const isMatch = yield user.comparePassword(req.body.password);
    if (isMatch) {
        console.log(req.user);
        return res.status(200).json({
            code: "0",
            message: "Servicio ejecutado con exito.",
            data: {
                id: user._id,
                email: user.email,
                nombre: user.nombre + " " + user.apellido,
                genero: user.genero,
                fecNac: user.fecha_nacimiento,
                telefono: user.telefono,
                doc: user.documento_ui,
                role: user.tipo,
                token: createToken(user)
            }
        });
    }
    return res.status(403).json({
        msg: "El correo o contrasena es incorrecto"
    });
});
/** TODOS LOS DOCTORES :: ASC | DESC */
exports.getAll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const filtro = req.params.filtro;
    let user;
    if (filtro == 'asc')
        user = yield user_1.default.find({}).populate("especialidades", "nombre").sort({ create_at: 1 });
    else
        user = yield user_1.default.find({}).populate("especialidades", "nombre").sort({ create_at: -1 });
    return res.status(200).json({
        code: "0",
        message: "Servicio ejecutado con exito.",
        data: user
    });
});
/** UN USUARIO :: ID */
exports.getById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.params.id;
    if (id.length < 24) {
        return res.status(400).json({ code: "404", message: "URL sin parametros para buscar." });
    }
    else {
        const user = yield user_1.default.findOne({ _id: id }).populate("especialidades", "nombre");
        if (user) {
            return res.status(200).json({
                code: "0",
                message: "Servicio ejecutado con exito.",
                data: user
            });
        }
        else {
            return res.status(404).json({ code: "404", message: "Usuario no fue encontrado o no existe." });
        }
    }
});
