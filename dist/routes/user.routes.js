"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_controller_1 = require("../controllers/user.controller");
const router = express_1.Router();
/** Todos los usuario :: ASC | DESC */
router.get('/doctores/:filtro', user_controller_1.getAll);
/** Un usuario por ID */
router.get('/doctores/find/by/:id', user_controller_1.getById);
exports.default = router;
