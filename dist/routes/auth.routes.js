"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_controller_1 = require("../controllers/user.controller");
const multer_1 = __importDefault(require("../libs/multer"));
const router = express_1.Router();
/** Verificacion ENVIAR SMS */
router.get('/sendverify', user_controller_1.enviarSMS);
/** Verificacion RECIBIR SMS */
router.get('/comprobarverify', user_controller_1.recibirSMS);
/** Registro :: La foto es opcional */
router.route('/signup').post(multer_1.default.fields([{ name: 'documentos', maxCount: 5 }, { name: 'foto', maxCount: 1 }]), user_controller_1.signUp);
/** Login :: Email & Password */
router.post('/signin', user_controller_1.signIn);
/** Update perfil :: La foto es opcional */
router.route('/perfil/update/:id').post(multer_1.default.fields([{ name: 'documentos', maxCount: 5 }, { name: 'foto', maxCount: 1 }]), user_controller_1.update);
/** Cambiar estado */
router.put('/perfil/update/status/:id', user_controller_1.updateStatus);
/** Enviar email */
router.post('/enviarmail/:nombre/:email', user_controller_1.verifySendEmail);
/** Recibir email */
router.post('/recibirmail/:email/:code', user_controller_1.verifyRecibirEmail);
/** Cambio de contraseña */
router.put('/cambio-contrasenia', user_controller_1.cambiarContrasenia);
exports.default = router;
