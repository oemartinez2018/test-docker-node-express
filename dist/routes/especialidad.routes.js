"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const router = express_1.Router();
const especialidades_controller_1 = require("../controllers/especialidades.controller");
/** Todas las especialidades */
router.get("/especialidades", especialidades_controller_1.getAll);
/** Registro de especialidad */
router.post("/especialidades", passport_1.default.authenticate("jwt", { session: false }), especialidades_controller_1.nuevo);
/** Update de especialidad */
router.put("/especialidades/:id", passport_1.default.authenticate("jwt", { session: false }), especialidades_controller_1.actualizar);
/** Eliminar especialidad */
router.delete("/especialidades/:id", passport_1.default.authenticate("jwt", { session: false }), especialidades_controller_1.eliminar);
exports.default = router;
