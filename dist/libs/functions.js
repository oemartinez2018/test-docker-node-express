"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodeMailer = require("nodemailer");
// Codigo generado para los correos
exports.generateCode = () => {
    const opciones = "abcdefghijklmnopqrstuvwxyz0123456789";
    let randonNumber = "";
    for (let index = 0; index < 6; index++) {
        randonNumber += opciones.charAt(Math.floor(Math.random() * opciones.length));
    }
    return randonNumber;
};
exports.sendEmail = (nombre, email, code) => {
    try {
        let transporter = nodeMailer.createTransport({
            service: "Hotmail",
            auth: {
                user: "jbarillasramirez@hotmail.com",
                pass: "thoneonejhbr1"
            }
        });
        let mailOptions = {
            from: "jbarillasramirez@hotmail.com",
            replyTo: "no-reply@solucionesroots.com",
            to: email,
            subject: "FONOMED✔!",
            html: `<p style="font-size: 16px;color: #808080"">¡Querido ${nombre}!<p>
                    <p style="font-size: 15px;color: #808080; line-height: 1.5;">Te saludamos de parte de Fonomed<br>
                    Para verificar tu cuenta por favor ingresa el siguiente codigo</p><br>
                    <center><h3>${code}</h3></center><br><br>

                    <p style="font-size: 12px;color: #808080">Att: Equipo de Fonomed</p>`
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log("Message %s sent: %s", info.messageId, info.response);
        });
        return true;
    }
    catch (err) {
        return false;
    }
};
