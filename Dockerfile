FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./

RUN  npm install  

RUN npm install -g typescript

COPY . ./dist

EXPOSE 3000

CMD [ "npm", "run", "dev" ]